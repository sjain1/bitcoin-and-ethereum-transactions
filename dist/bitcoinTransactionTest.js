"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/web.dom.iterable");

var _readlineFile = _interopRequireDefault(require("./readlineFile"));

var _bigNumber = _interopRequireDefault(require("big-number"));

var _enumFile = require("./enumFile");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Request = require("request-promise");

const bitcoin = require('bitcoinjs-lib');

require('dotenv').load();

const baseURL = "https://testnet.blockexplorer.com/api/";
var totalAmount = 0;
let NETWORK;

if (process.env.NODE_ENVIRONMENT === _enumFile.environment.development.value) {
  NETWORK = bitcoin.networks.testnet;
} ///------------------senderAddress------------


function getSenderAddress(address) {
  return Request(`${baseURL}addr/${address}/balance`).then(function (body) {
    return body;
  }).catch(function (error) {
    console.log(error.message);
  });
} ///---------------getutxo-------------------


function getUtxos(address) {
  //   return Request(`${baseURL}addr/${address}/utxo`)
  //   .then(function (body) {
  //       return JSON.parse(body);
  //   })
  //   .catch(function (error) {
  //       console.log(error.message)
  //       return;
  //   }); //console.log(utxo);
  var options = {
    method: 'get',
    uri: `https://testnet-api.smartbit.com.au/v1/blockchain/address/${address}/unspent`,
    body: {
      some: 'payload'
    },
    json: true // Automatically stringifies the body to JSON

  };
  return Request(options).then(function (parsedBody) {
    return parsedBody;
  }).catch(function (error) {
    console.log(error);
  });
} //https://testnet.blockexplorer.com/api/addr/mtrEcW8BpdCYae9BPo6E8wmiQAmR8HMH3L/utxo?noCache=1
///------------------utxo selection---------------------


function utxoSelection(utxo, target) {
  utxo.sort(function (a, b) {
    return a.value_int - b.value_int;
  }); //console.log(utxo);

  var utxoFound = false;
  var selectedUtxo = [];

  for (let i = 0; i < utxo.length; i++) {
    if (utxo[i].value_int >= target) {
      selectedUtxo.push(utxo[i]);
      totalAmount = (0, _bigNumber.default)(totalAmount).plus(utxo[i].value_int); //totalAmount += utxo[i].satoshis;

      utxoFound = true;
      break;
    }
  }

  let size = utxo.length;

  if (utxoFound === false) {
    let lastutxo = (0, _bigNumber.default)(utxo[utxo.length - 1].value_int);

    while (utxoFound === false) {
      for (let i = 0; i < size - 1; i++) {
        let amount = (0, _bigNumber.default)(lastutxo).plus(utxo[i].value_int); //lastutxo + utxo[i].satoshis;

        if (amount >= target) {
          selectedUtxo.push(utxo[i]);
          totalAmount = (0, _bigNumber.default)(totalAmount).plus(utxo[i].value_int);
          utxoFound = true;

          for (let k = size - 1; k < utxo.length; k++) {
            selectedUtxo.push(utxo[k]);
            totalAmount = (0, _bigNumber.default)(totalAmount).plus(utxo[i].value_int);
          }

          break;
        }
      }

      if (utxoFound === true || size < 0) break;
      size--;
      lastutxo = (0, _bigNumber.default)(lastutxo).plus(utxo[size - 1].value_int); //+= utxo[size-1].satoshis;
    }
  }

  return selectedUtxo;
} ///---------------sendTransaction---------------


function sendBitcoinTransaction(transactionHex) {
  var sendTransactionsOptions = {
    method: 'POST',
    uri: `${baseURL}tx/send`,
    //`https://testnet.blockexplorer.com/api/tx/send?rawtx=${transaction.toHex()}`,
    body: {
      rawtx: transactionHex
    },
    json: true // Automatically stringifies the body to JSON

  };
  Request(sendTransactionsOptions).then(function (body) {
    console.log(body);
  }).catch(function (error) {
    console.log(error.message);
  });

  _readlineFile.default.close();
} ///-----------------createTransaction------------------------------------


function createTransaction(selectedUtxo, target, toAddress, fromaddress, keypair) {
  const bitcoinTransaction = new bitcoin.TransactionBuilder(NETWORK);
  bitcoinTransaction.setVersion(2);
  selectedUtxo.forEach(input => {
    bitcoinTransaction.addInput(input.txid, input.n);
  });
  let fees = 2260;
  let change = totalAmount - target - fees;
  bitcoinTransaction.addOutput(toAddress, target);
  bitcoinTransaction.addOutput(fromaddress, change);
  bitcoinTransaction.sign(0, keypair);
  let transaction = bitcoinTransaction.build();
  var transactionHex = transaction.toHex();
  console.log(transactionHex);
  sendBitcoinTransaction(transactionHex);
} ///---------------transactionMainFunction---------------------------------


async function bitcoinTransaction(privatekey, toAddress, amount) {
  try {
    bitcoin.address.toOutputScript(toAddress, NETWORK);
    let keypair = bitcoin.ECPair.fromWIF(privatekey, NETWORK); //makeRandom({networt:testnet});

    let _bitcoin$payments$p2p = bitcoin.payments.p2pkh({
      pubkey: keypair.publicKey,
      network: NETWORK
    }),
        address = _bitcoin$payments$p2p.address;

    let target = parseInt(amount, 10);
    var balance = await getSenderAddress(address);

    if (balance < target) {
      console.log("Sender doesn't have enough amount to send");
      return;
    }

    console.log(balance);
    var utxo = await getUtxos(address); //console.log(utxo);

    if (utxo == null) {
      console.log("Utxos are not in json formate");
      return;
    }

    console.log(utxo.unspent);
    var selectedUtxo = [];
    selectedUtxo = utxoSelection(utxo.unspent, target);

    if (selectedUtxo == null) {
      console.log("No utxo matched");
      return;
    }

    createTransaction(selectedUtxo, target, toAddress, address, keypair);
  } catch (error) {
    console.log(error.message);
  }
}

var _default = bitcoinTransaction;
exports.default = _default;