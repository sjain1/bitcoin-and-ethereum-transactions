"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _readline = _interopRequireDefault(require("readline"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var readLineModule = function readLineModule() {
  return _readline.default.createInterface({
    input: process.stdin,
    output: process.stdout
  });
};

var _default = readLineModule();

exports.default = _default;