"use strict";

const bitcoin = require("bitcoinjs-lib");

const TESTNET = bitcoin.networks.testnet; // console.log(TESTNET);

const keyPair = bitcoin.ECPair.makeRandom({
  network: TESTNET
});

const _bitcoin$payments$p2p = bitcoin.payments.p2pkh({
  pubkey: keyPair.publicKey,
  network: TESTNET
}),
      address = _bitcoin$payments$p2p.address;

console.log("rec: ", address);
const alice = bitcoin.ECPair.fromWIF("Kz8ffKQRRoLRHaFebu1Fibhh9Tggk7vz4FKQDFxHi1rTT8C6mGVf");
const abc = bitcoin.payments.p2pkh({
  pubkey: alice.publicKey,
  network: TESTNET
});
console.log("sen:", abc.address);
const txb = new bitcoin.TransactionBuilder(TESTNET);
txb.setVersion(1);
txb.addInput("228d978bedc08063be43b5786287cfb09a02472e779771f39b2ecd1cf7beca8b", 0); // Alice's previous transaction output, has 15000 satoshis

txb.addOutput(address, 12000); // (in)15000 - (out)12000 = (fee)3000, this is the miner fee

console.log(alice);
alice.network = TESTNET;
txb.sign(0, alice);
console.log(txb.build().toHex());