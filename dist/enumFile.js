"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.environment = exports.blockchainsType = void 0;

var _enum = _interopRequireDefault(require("enum"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var blockchainsType = new _enum.default({
  'BITCOIN': 0,
  'ETHEREUM': 60
});
exports.blockchainsType = blockchainsType;
var environment = new _enum.default({
  'development': "development"
});
exports.environment = environment;