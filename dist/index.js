"use strict";

var _readlineFile = _interopRequireDefault(require("./readlineFile"));

var _ethereumTransaction = _interopRequireDefault(require("./ethereumTransaction"));

var _bitcoinTransaction = _interopRequireDefault(require("./bitcoinTransaction"));

var _enumFile = require("./enumFile");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_readlineFile.default.question('Enter the transaction type, bitcoin or ethereum: ', transactionType => {
  transactionType = transactionType.toUpperCase();

  switch (transactionType) {
    case _enumFile.blockchainsType.BITCOIN.key:
      bitcoinTransactionInput();
      break;

    case _enumFile.blockchainsType.ETHEREUM.key:
      ethereumTransactionInput();
      break;

    default:
      console.log("Enter valid transaction type");

      _readlineFile.default.close();

  }
});

function ethereumTransactionInput() {
  _readlineFile.default.question('Enter your private key: ', privateKey => {
    _readlineFile.default.question('Enter sender address: ', toAddress => {
      _readlineFile.default.question('Enter amount to be send decimal: ', amount => {
        (0, _ethereumTransaction.default)(privateKey, toAddress, amount);
      });
    });
  });
}

function bitcoinTransactionInput() {
  _readlineFile.default.question('Enter your private key: ', privateKey => {
    _readlineFile.default.question('Enter sender address: ', toAddress => {
      _readlineFile.default.question('Enter amount to be send in satoshis: ', amount => {
        (0, _bitcoinTransaction.default)(privateKey, toAddress, amount);
      });
    });
  });
}