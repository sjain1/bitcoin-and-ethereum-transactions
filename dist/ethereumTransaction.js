"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.regexp.to-string");

var _readlineFile = _interopRequireDefault(require("./readlineFile"));

var _ethereumjsTx = _interopRequireDefault(require("ethereumjs-tx"));

var _web = _interopRequireDefault(require("web3"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const ethUtil = require("ethereumjs-util");

let web3 = new _web.default(new _web.default.providers.HttpProvider("https://ropsten.infura.io/v3/e68eb31d4fa944aaa359ea31af9b0db8"));

var EthereumTransaction = (privateKey, senderAddress, amount) => {
  if (web3.isAddress(senderAddress) && privateKey.length == 64) {
    //console.log("valid Address");
    let privateKeyBuffer = Buffer.from(privateKey, "hex");
    let address = "0x" + ethUtil.privateToAddress(privateKeyBuffer).toString("hex");
    const transactionParams = {
      nonce: web3.toHex(web3.eth.getTransactionCount(address)),
      gasPrice: web3.toHex(web3.eth.gasPrice.toString()),
      gasLimit: web3.toHex(300000),
      to: senderAddress,
      value: web3.toHex(amount),
      data: "0x00",
      chainId: 3
    };
    const transaction = new _ethereumjsTx.default(transactionParams);
    transaction.sign(privateKeyBuffer);
    const serializedTransaction = transaction.serialize();
    web3.eth.sendRawTransaction('0x' + serializedTransaction.toString('hex'), function (err, hash) {
      if (!err) console.log(hash);else {
        console.log(err);
      }
    });
    web3.eth.getBalance(address, (err, bal) => {
      if (!err) {
        console.log(bal.toNumber());
      } else {
        console.log(err);
      }
    });
  } else {
    console.log("Invalid Address");
  }

  _readlineFile.default.close();
};

var _default = EthereumTransaction;
exports.default = _default;