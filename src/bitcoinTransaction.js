import readLineModule from "./readlineFile";
import BigNumber from 'big-number';
import { environment } from './enumFile';
var Request = require("request-promise");
const bitcoin = require('bitcoinjs-lib');
require('dotenv').load();

const baseURL = "https://testnet.blockexplorer.com/api/";
var totalAmount = 0;
let NETWORK;
if (process.env.NODE_ENVIRONMENT === environment.development.value) {
  NETWORK  = bitcoin.networks.testnet;
}

///------------------senderAddress------------
function getSenderAddress(address) {
  return Request(`${baseURL}addr/${address}/balance`)
  .then(function (body) {
      return body;
  })
  .catch(function (error) {
      console.log(error.message)
  });
}

///---------------getutxo-------------------
function getUtxos(address) {
  var options = {
    method: 'get',
    uri: `https://testnet-api.smartbit.com.au/v1/blockchain/address/${address}/unspent`,
    body: {
        some: 'payload'
    },
    json: true // Automatically stringifies the body to JSON
  };

  return Request(options)
  .then(function (parsedBody) {
      return parsedBody;
  })
  .catch(function (error) {
      console.log(error)
  });
}

///------------------utxo selection---------------------
function utxoSelection(utxo, target) {
  utxo.sort(function(a, b) {
    return a.value_int - b.value_int;
  }); //console.log(utxo);
  
  var utxoFound = false;
  var selectedUtxo = [];

  for(let i = 0; i < utxo.length; i++) {
    if(utxo[i].value_int >= target) {
      selectedUtxo.push(utxo[i]);
      totalAmount = BigNumber(totalAmount).plus(utxo[i].value_int)
      utxoFound = true;
      break;
    } 
  }

  let size = utxo.length;
  if(utxoFound === false) {
    let lastutxo = BigNumber(utxo[utxo.length-1].value_int);
    while(utxoFound === false) {
      for(let i = 0; i < size - 1; i++) {
        let amount = BigNumber(lastutxo).plus(utxo[i].value_int);
        if(amount >= target) {
          selectedUtxo.push(utxo[i]);
          totalAmount = BigNumber(totalAmount).plus(utxo[i].value_int);
          utxoFound = true;
          
          for(let k = size - 1; k < utxo.length; k++) {
            selectedUtxo.push(utxo[k]);
            totalAmount = BigNumber(totalAmount).plus(utxo[i].value_int);
          }
          break;
        } 
      }
      if(utxoFound === true || size < 0)
        break;
      
      size--;
      lastutxo = BigNumber(lastutxo).plus(utxo[size-1].value_int);
    } 
  }
  return selectedUtxo;
}

///---------------sendTransaction---------------
function sendBitcoinTransaction(transactionHex) {
  var sendTransactionsOptions = {
    method: 'POST',
    uri: `${baseURL}tx/send`,
    body: {
      rawtx: transactionHex
    },
    json: true // Automatically stringifies the body to JSON
  };

  Request(sendTransactionsOptions)
  .then(function (body) {
    console.log(body);
  })
  .catch(function (error) {
      console.log(error.message)
  });
  readLineModule.close();
}

///-----------------createTransaction------------------------------------
function createTransaction(selectedUtxo, target, toAddress, fromaddress, keypair) {
  const bitcoinTransaction = new bitcoin.TransactionBuilder(NETWORK);
  
  bitcoinTransaction.setVersion(2);
  selectedUtxo.forEach(input => {
    bitcoinTransaction.addInput(input.txid, input.n);
  });
  
  let fees = 2260;
  let change = totalAmount-target-fees;
  
  bitcoinTransaction.addOutput(toAddress, target);
  bitcoinTransaction.addOutput(fromaddress, change);
  bitcoinTransaction.sign(0, keypair);
  
  let transaction = bitcoinTransaction.build();
  var transactionHex = transaction.toHex();

  console.log(transactionHex);
  
  sendBitcoinTransaction(transactionHex);
}

///---------------transactionMainFunction---------------------------------
async function bitcoinTransaction(privatekey, toAddress, amount) {
  try {
    bitcoin.address.toOutputScript(toAddress, NETWORK);

    let keypair = bitcoin.ECPair.fromWIF(privatekey, NETWORK)
    let { address } = bitcoin.payments.p2pkh({ pubkey: keypair.publicKey, network: NETWORK });

    let target = parseInt(amount, 10);
  
    var balance = await getSenderAddress(address);
    
    if(balance < target) {
      console.log("Sender doesn't have enough amount to send")
      return;
    }
    //console.log(balance);
    var utxo = await getUtxos(address);//console.log(utxo);
    if(utxo === null) {
      console.log("Utxos are not in json formate");
      return;
    }
    //console.log(utxo.unspent);
    
    var selectedUtxo = [];
    selectedUtxo = utxoSelection(utxo.unspent, target);
  
    if(selectedUtxo === null) {
      console.log("No utxo matched")
      return;
    }
    createTransaction(selectedUtxo, target, toAddress, address, keypair);
  } catch(error) {
    console.log(error.message);
  }
}

export default bitcoinTransaction;
