import readline from 'readline';
var readLineModule = function () {
  return readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
}

export default readLineModule();
