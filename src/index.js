import readLineModule from "./readlineFile";
import EthereumTransaction from './ethereumTransaction';
import bitcoinTransaction from './bitcoinTransaction';
import { blockchainsType } from './enumFile';

readLineModule.question('Enter the transaction type, bitcoin or ethereum: ', (transactionType) => {
  transactionType = transactionType.toUpperCase();
  
  switch(transactionType) {
    case blockchainsType.BITCOIN.key: bitcoinTransactionInput();
      break;
    case blockchainsType.ETHEREUM.key: ethereumTransactionInput();
      break;
    default: console.log("Enter valid transaction type");
      readLineModule.close();
  }
});

function ethereumTransactionInput() {
  readLineModule.question('Enter your private key: ', (privateKey) => {
    readLineModule.question('Enter sender address: ', (toAddress) => {
      readLineModule.question('Enter amount to be send decimal: ', (amount) => {
        EthereumTransaction(privateKey, toAddress, amount);
      });
    });
  });  
}

function bitcoinTransactionInput() {
  readLineModule.question('Enter your private key: ', (privateKey) => {
    readLineModule.question('Enter sender address: ', (toAddress) => {
      readLineModule.question('Enter amount to be send in satoshis: ', (amount) => {
        bitcoinTransaction(privateKey, toAddress, amount);
      });
    });
  });  
}

