import readLineModule from "./readlineFile";
const ethUtil = require("ethereumjs-util");
import ethereumTransaction from "ethereumjs-tx";
import Web3 from "web3";

let web3 = new Web3(
  new Web3.providers.HttpProvider(
    "https://ropsten.infura.io/v3/e68eb31d4fa944aaa359ea31af9b0db8"
  )
);

var EthereumTransaction = (privateKey, senderAddress, amount) => {
  if (web3.isAddress(senderAddress) && privateKey.length == 64) {
    //console.log("valid Address");
    let privateKeyBuffer = Buffer.from(privateKey, "hex");
    let address = "0x" + ethUtil.privateToAddress(privateKeyBuffer).toString("hex");

    const transactionParams = {
      nonce: web3.toHex(web3.eth.getTransactionCount(address)),
      gasPrice: web3.toHex(web3.eth.gasPrice.toString()),
      gasLimit: web3.toHex(300000),
      to: senderAddress,
      value: web3.toHex(amount),
      data: "0x00",
      chainId: 3
    };

    const transaction = new ethereumTransaction(transactionParams);
    transaction.sign(privateKeyBuffer);
    const serializedTransaction = transaction.serialize();

    web3.eth.sendRawTransaction('0x' + serializedTransaction.toString('hex'), function(err, hash) {
      if (!err)
        console.log(hash);
      else {
        console.log(err);
      }
    });

    web3.eth.getBalance(address, (err, bal) => {
      if (!err) {
        console.log(bal.toNumber());
      } else {
        console.log(err);
      }
    });
  } else {
    console.log("Invalid Address");
  }

  readLineModule.close();
};

 export default EthereumTransaction;
