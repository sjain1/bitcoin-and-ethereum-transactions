import Enum from 'enum';

var blockchainsType = new Enum({ 'BITCOIN': 0, 'ETHEREUM': 60 });
var environment = new Enum({ 'development': "development" });

export { blockchainsType, environment };